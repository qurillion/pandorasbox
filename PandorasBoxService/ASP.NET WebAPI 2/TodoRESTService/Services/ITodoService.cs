﻿using System.Collections.Generic;
using TodoRESTService.Models;

namespace TodoRESTService.Services
{
    public interface ITodoService
    {
        bool DoesItemExist(string id);
        Box Find(string id);
        IEnumerable<Box> GetData();
        void InsertData(Box item);
        void UpdateData(Box item);
        void DeleteData(string id);
    }
}
