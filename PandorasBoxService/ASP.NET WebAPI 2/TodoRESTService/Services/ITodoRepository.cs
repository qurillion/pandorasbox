﻿using System.Collections.Generic;
using TodoRESTService.Models;

namespace TodoRESTService.Services
{
    public interface ITodoRepository
    {
        bool DoesItemExist(string id);
        IEnumerable<Box> All { get; }
        Box Find(string id);
        void Insert(Box item);
        void Update(Box item);
        void Delete(string id);
    }
}
