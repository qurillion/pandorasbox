﻿using System.Collections.Generic;
using System.Linq;
using TodoRESTService.Models;

namespace TodoRESTService.Services
{
    public class TodoRepository : ITodoRepository
    {
        private List<Box> _todoList;

        public TodoRepository()
        {
            InitializeData();
        }

        public IEnumerable<Box> All
        {
            get { return _todoList; }
        }

        public bool DoesItemExist(string id)
        {
            return _todoList.Any(item => item.ID == id);
        }

        public Box Find(string id)
        {
            return _todoList.Where(item => item.ID == id).FirstOrDefault();
        }

        public void Insert(Box item)
        {
            _todoList.Add(item);
        }

        public void Update(Box item)
        {
            var todoItem = this.Find(item.ID);
            var index = _todoList.IndexOf(todoItem);
            _todoList.RemoveAt(index);
            _todoList.Insert(index, item);
        }

        public void Delete(string id)
        {
            _todoList.Remove(this.Find(id));
        }

        #region Helpers

        private void InitializeData()
        {
            _todoList = new List<Box>();

            var todoItem1 = new Box {
                ID = "6bb8a868-dba1-4f1a-93b7-24ebce87e243",
                Name = "Box 1",
                Item0 = "Shirts",
                Item1 = "Shorts",
                Item2 = "Socks",
                Room = "Bedroom",
                Function = "Clothes",
                Done = false
            };

            var todoItem2 = new Box {
                ID = "b94afb54-a1cb-4313-8af3-b7511551b33b",
                Name = "Box 2",
                Item0 = "Mice",
                Item1 = "Keyboards",
                Item2 = "USB Cables",
                Room = "Computer Room",
                Function = "Computers",
                Done = false
            };

            var todoItem3 = new Box {
                ID = "ecfa6f80-3671-4911-aabe-63cc442c1ecf",
                Name = "Box 3",
                Item0 = "Books",
                Item1 = "Games",
                Item2 = "DVDs",
                Room = "Television Room",
                Function = "Entertainment",
                Done = false,
            };

            var todoItem4 = new Box {
                ID = "acfa8f80-3671-4911-aabe-63cc442c1ecf",
                Name = "Box 4",
                Item0 = "Sheets",
                Item1 = "Blankets",
                Item2 = "Pillow Covers",
                Room = "Storage Room",
                Function = "Linen",
                Done = false,
            };

            var todoItem5 = new Box {
                ID = "xcfa8f80-3681-4911-aabe-63cc442c1ecf",
                Name = "Box 5",
                Item0 = "Spoons",
                Item1 = "Forks",
                Item2 = "Knives",
                Room = "Kitchen",
                Function = "Cutlery",
                Done = false,
            };

            var todoItem6 = new Box {
                ID = "hcfm8f80-3681-4911-aabe-63cc442c1ecf",
                Name = "Box 6",
                Item0 = "Sugar",
                Item1 = "Tea",
                Item2 = "Coffee",
                Room = "Pantry",
                Function = "Ingredients",
                Done = false,
            };

            var todoItem7 = new Box {
                ID = "hcfm8f80-5731-4911-aabe-63cc442c1ecf",
                Name = "Box 7",
                Item0 = "Sauce",
                Item1 = "Salt",
                Item2 = "Pepper",
                Room = "Dining Room",
                Function = "Condiments",
                Done = false,
            };

            var todoItem8 = new Box {
                ID = "yndm8f80-5731-4911-aabe-63cc442c1ecf",
                Name = "Box 8",
                Item0 = "Razor",
                Item1 = "Toothpaste",
                Item2 = "Toothbrush",
                Room = "Bathroom",
                Function = "Hygiene",
                Done = false,
            };

            _todoList.Add(todoItem1);
            _todoList.Add(todoItem2);
            _todoList.Add(todoItem3);
            _todoList.Add(todoItem4);
            _todoList.Add(todoItem5);
            _todoList.Add(todoItem6);
            _todoList.Add(todoItem7);
            _todoList.Add(todoItem8);
        }

        #endregion
    }
}
