﻿using System.Collections.Generic;

namespace TodoRESTService.Models
{
    public class Box {
        public string ID { get; set; }

        public string Name { get; set; }

        public string Item0 { get; set; }

        public string Item1 { get; set; }

        public string Item2 { get; set; }

        public string Item3 { get; set; }

        public string Item4 { get; set; }

        public string Item5 { get; set; }

        public string Item6 { get; set; }

        public string Item7 { get; set; }

        public string Item8 { get; set; }

        public string Item9 { get; set; }

        public string Room { get; set; }

        public string Function { get; set; }

        public bool Done { get; set; }
    }
}