﻿using System;
using Xamarin.Forms;

namespace PandorasBox {
    public partial class BoxListPage : ContentPage {

        public BoxListPage() {
            InitializeComponent();
        }

        protected async override void OnAppearing() {
            // when the page appears
            base.OnAppearing();
            // the ListView named "listView" will display all items from GetTasksAsync
            listView.ItemsSource = await App.BoxManager.GetTasksAsync();
        }

        void OnAddItemClicked(object sender, EventArgs e) {
            var box = new Box() {
                ID = Guid.NewGuid().ToString()
            };
            var boxPage = new BoxPage(true);
            boxPage.BindingContext = box;
            Navigation.PushAsync(boxPage);
        }

        void OnItemSelected(object sender, SelectedItemChangedEventArgs e) {

            // store the clicked on box
            var box = e.SelectedItem as Box;

            // create a new page to display the contents
            var boxPage = new BoxPage();

            // bind the box properties to the new page
            boxPage.BindingContext = box;

            // show the new page
            Navigation.PushAsync(boxPage);
        }
    }
}
