﻿using System;
using Xamarin.Forms;

namespace PandorasBox {
    public partial class SearchPage : ContentPage {


        public SearchPage() {
            InitializeComponent();
            BindingContext = new SearchPageViewModel();

        }

        void OnListViewItemTapped(object sender, ItemTappedEventArgs e) {
            ((ListView)sender).SelectedItem = null;
        }

        async void OnListViewItemSelected(object sender, SelectedItemChangedEventArgs e) {
            // save the clicked on item
            var box = ((ListView)sender).SelectedItem as Box;

            if (box != null) {
                // create a new page to display the contents
                var boxPage = new BoxPage();
                // bind the item to the new page
                boxPage.BindingContext = box;

                // show the new page
                await Navigation.PushAsync(boxPage);
            }
        }


        // Only uncomment below to confirm listview is displaying results correctly
        //protected async override void OnAppearing() {
        //    base.OnAppearing();

        //    listView.ItemsSource = await App.TodoManager.GetTasksAsync();
        //}


    }
}
