﻿using System;
using Xamarin.Forms;

namespace PandorasBox {
    public partial class RoomListPage : ContentPage {

        public RoomListPage() {
            InitializeComponent();
        }

        protected async override void OnAppearing() {
            base.OnAppearing();

            listView.ItemsSource = await App.BoxManager.GetTasksAsync();
        }

        void OnAddItemClicked(object sender, EventArgs e) {
            var box = new Box() {
                ID = Guid.NewGuid().ToString()
            };
            var boxPage = new BoxPage(true);
            boxPage.BindingContext = box;
            Navigation.PushAsync(boxPage);
        }

        void OnItemSelected(object sender, SelectedItemChangedEventArgs e) {
            var box = e.SelectedItem as Box;
            var boxPage = new BoxPage();
            boxPage.BindingContext = box;
            Navigation.PushAsync(boxPage);
        }
    }
}
