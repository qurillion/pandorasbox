﻿using System;
using Xamarin.Forms;

namespace PandorasBox {
    public partial class BoxPage : ContentPage {
        bool isNewBox;

        public BoxPage(bool isNew = false) {
            InitializeComponent();
            isNewBox = isNew;
        }

        async void OnSaveActivated(object sender, EventArgs e) {
            var box = (Box)BindingContext;
            await App.BoxManager.SaveTaskAsync(box, isNewBox);
            await Navigation.PopAsync();
        }

        async void OnDeleteActivated(object sender, EventArgs e) {
            var box = (Box)BindingContext;
            await App.BoxManager.DeleteTaskAsync(box);
            await Navigation.PopAsync();
        }

        void OnCancelActivated(object sender, EventArgs e) {
            Navigation.PopAsync();
        }

        void OnSpeakActivated(object sender, EventArgs e) {
            var box = (Box)BindingContext;
        }
    }
}
