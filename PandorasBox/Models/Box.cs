﻿using System;
using System.Collections.Generic;

namespace PandorasBox {
    public class Box {
        public string ID { get; set; }

        public string Name { get; set; }

        public string Item0 { get; set; }

        public string Room { get; set; }

        public string Function { get; set; }

        public bool Done { get; set; }
    }
}
