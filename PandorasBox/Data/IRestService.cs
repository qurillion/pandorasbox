﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace PandorasBox {
    public interface IRestService {
        Task<List<Box>> RefreshDataAsync();

        Task SaveTodoItemAsync(Box item, bool isNewItem);

        Task DeleteTodoItemAsync(string id);
    }
}
