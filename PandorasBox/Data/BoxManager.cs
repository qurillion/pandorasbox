﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PandorasBox {
    public class BoxManager {
        IRestService restService;

        public BoxManager(IRestService service) {
            restService = service;
        }

        public Task<List<Box>> GetTasksAsync() {
            return restService.RefreshDataAsync();
        }

        public Task SaveTaskAsync(Box box, bool isNewBox = false) {
            return restService.SaveTodoItemAsync(box, isNewBox);
        }

        public Task DeleteTaskAsync(Box box) {
            return restService.DeleteTodoItemAsync(box.ID);
        }
    }
}
