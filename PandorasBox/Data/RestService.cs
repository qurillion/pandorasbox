﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PandorasBox {
    public class RestService : IRestService {
        HttpClient client;

        public List<Box> AllBoxes { get; private set; }

        public RestService() {
            var authData = string.Format("{0}:{1}", Constants.Username, Constants.Password);
            var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
        }

        public async Task<List<Box>> RefreshDataAsync() {
            AllBoxes = new List<Box>();

            // RestUrl = http://developer.xamarin.com:8081/api/todoitems
            var uri = new Uri(string.Format(Constants.RestUrl + "api/todoitems/", string.Empty));

            try {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode) {
                    var content = await response.Content.ReadAsStringAsync();
                    AllBoxes = JsonConvert.DeserializeObject<List<Box>>(content);
                }
            } catch (Exception ex) {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
            }

            return AllBoxes;
        }

        public async Task SaveTodoItemAsync(Box box, bool isNewItem = false) {
            // RestUrl = http://developer.xamarin.com:8081/api/todoitems
            var uri = new Uri(string.Format(Constants.RestUrl + "api/todoitems/", string.Empty));

            try {
                var json = JsonConvert.SerializeObject(box);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;
                if (isNewItem) {
                    response = await client.PostAsync(uri, content);
                } else {
                    response = await client.PutAsync(uri, content);
                }

                if (response.IsSuccessStatusCode) {
                    Debug.WriteLine(@"Box successfully saved.");
                }

            } catch (Exception ex) {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
            }
        }

        public async Task DeleteTodoItemAsync(string id) {
            // RestUrl = http://developer.xamarin.com:8081/api/todoitems/{0}
            var uri = new Uri(string.Format(Constants.RestUrl + "api/todoitems/" + id));

            try {
                var response = await client.DeleteAsync(uri);

                if (response.IsSuccessStatusCode) {
                    Debug.WriteLine(@"Box successfully deleted.");
                }

            } catch (Exception ex) {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
            }
        }
    }
}
