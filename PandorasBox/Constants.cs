﻿namespace PandorasBox {
    public static class Constants {
        // URL of REST service
        public static string RestUrl = "http://pandorasboxrest.azurewebsites.net/";
        // Credentials that are hard coded into the REST service
        public static string Username = "user";
        public static string Password = "pass";
    }
}
