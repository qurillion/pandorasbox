﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace PandorasBox {
    public class App : Application {
        public static BoxManager BoxManager { get; private set; }

        public App() {
            // App starts a new Manager to begin accessing items
            BoxManager = new BoxManager(new RestService());
            //MainPage = new NavigationPage (new TodoListPage ());
            MainPage = new NavigationPage(new StartPage());

        }

        protected override void OnStart() {
            // Handle when your app starts
        }

        protected override void OnSleep() {
            // Handle when your app sleeps
        }

        protected override void OnResume() {
            // Handle when your app resumes
        }
    }
}

