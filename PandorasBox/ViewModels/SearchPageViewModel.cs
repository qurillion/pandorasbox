﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PandorasBox
{
    public class SearchPageViewModel {

        public ObservableCollection<Box> ResultsList { get; set; }

        public ICommand SearchCommand { get; set; }



        // SearchCommand is bound to the search bar and calls the search based on the query when the user hits search
        public SearchPageViewModel() {
            
            SearchCommand = new Command<string>(async (text) => await SearchItems(text));
            ResultsList = new ObservableCollection<Box>();
        }



        // search all items returned from the REST service
        async Task SearchItems(string text) {

            ResultsList.Clear();

            // insert REST service items into a list
            List<Box> allBoxes = await App.BoxManager.GetTasksAsync();

            // loop through items that match add to public variable
            foreach (Box box in allBoxes) {
                if (box.Name.Contains(text) || 
                    box.Item0.Contains(text) ||
                    box.Room.Contains(text) || 
                    box.Room.Contains(text) ) {
                    ResultsList.Add(box);
                }  
            }



        }
    }
}
